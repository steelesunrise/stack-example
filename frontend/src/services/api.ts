import { Token } from "../types/Me";

const apiUrl: string | undefined = process.env.REACT_APP_API_URL;

class APIError extends Error {
  originalError: any;
  constructor(message: string, error: any) {
    super(message);
    this.name = "APIError";
    this.originalError = error;
  }
}

// Set up headers and make api call
// return Api response
async function apiCallBase(
  method: string,
  route: string,
  data: any = null
): Promise<Response> {
  const req: RequestInit = {
    method,
    headers: new Headers({})
  };

  let token = localStorage.getItem("token");
  if (token) {
    (req.headers as Headers).append("Authorization", token);
  }

  if (data !== null && data !== undefined) {
    req.body = JSON.stringify(data);
    (req.headers as Headers).append("Content-Type", "application/json");
  }

  let res: Response;
  try {
    res = await fetch(apiUrl + route, req);
  } catch (err) {
    throw new APIError("Network error", err);
  }
  return res;
}

// Make api call and handle response
async function apiCall<T = any>(
  method: string,
  route: string,
  data: any = null
): Promise<T> {
  let res = await apiCallBase(method, route, data);
  let resBody: any = await res.json();
  console.log("Response", resBody);
  return resBody.data as T;

  //   let resBody: any;
  //   try {
  //     resBody = await res.json();

  //     // Normally our api returns a success flag. Return after success
  //     if (resBody.success) {
  //       return resBody.data as T;
  //     }
  //   } catch (err) {
  //     throw new APIError("Bad error", err);
  //   }

  //   // Depending on how api returns errors handle your errors here.
  //   let message = "Unexpected Error";

  //   const error = (resBody && resBody.error) || {};
  //   if (typeof error.message === "string" && error.message.trim().length > 0) {
  //     message = error.message;
  //   } else if (typeof error.msg === "string" && error.msg.trim().length > 0) {
  //     message = error.msg;
  //   }

  //   switch (resBody && resBody.error && resBody.error.type) {
  //     case "NotAuthenticatedError":
  //       // Handle NotAuthenticatedError
  //       break;
  //   }

  //   throw new APIError(message, resBody.error);
}

function post<T = any>(urlExtension: string, data: any): Promise<T> {
  return apiCall<T>("POST", urlExtension, data);
}

function get<T = any>(urlExtension: string): Promise<T> {
  return apiCall<T>("GET", urlExtension);
}

export async function signIn(username: string, password: string) {
  let res = await post<Token>("auth/login", {
    username: username,
    password: password
  });
  localStorage.setItem("token", res.token);
  return res;
}

export function call() {
  return get<any>("todos/1");
}
