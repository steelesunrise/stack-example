import React from "react";
import AppRouter from "./AppRouter";
import { MuiThemeProvider } from "@material-ui/core";
import { theme } from "./styles";

const App: React.FC = () => {
  // console.log(process.env);
  if (process.env.REACT_APP_USE_CUSTOM_THEME) {
    return (
      <MuiThemeProvider theme={theme}>
        <AppRouter></AppRouter>
      </MuiThemeProvider>
    );
  } else {
    return <AppRouter></AppRouter>;
  }
};

export default App;
