import React from "react";
import { Box, Typography, Button, TextField } from "@material-ui/core";
import "./LoginPage.css";
import { call } from "../../services/api";

export default function LoginPage() {
  const [username, setUsername] = React.useState<string>("");
  const [password, setPassword] = React.useState<string>("");
  const makeBlue = false;

  function passwordOnChange(event: any) {
    setPassword(event.target.value);
  }

  async function login() {
    const res = await call();
    console.log("API RES", res);
  }

  return (
    <Box className={"background"}>
      <Box className={"panel" + (makeBlue ? " blue" : "")}>
        <Box className={"bottom-m"}>
          <Typography variant="h1">LOGIN PAGE</Typography>
        </Box>
        <Box className={"bottom-m top-m"}>
          <TextField
            type="text"
            value={username}
            // Using fat arrow syntax
            onChange={event => {
              setUsername(event.target.value);
            }}
            variant="outlined"
            label="Username"
            margin="dense"
            fullWidth
          />
        </Box>
        <Box className={"bottom-m top-m"}>
          <TextField
            type="password"
            value={password}
            onChange={passwordOnChange}
            variant="outlined"
            label="Password"
            margin="dense"
            fullWidth
          />
        </Box>
        <Box className={"top-m"}>
          <Button
            variant="contained"
            color="primary"
            size="large"
            onClick={login}
          >
            Log In
          </Button>
        </Box>
      </Box>
    </Box>
  );
}
