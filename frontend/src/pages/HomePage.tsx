import { Typography, Box } from "@material-ui/core";
import React from "react";

export default function HomePage() {
  return (
    <Box>
      <Typography>Home Page</Typography>
    </Box>
  );
}
