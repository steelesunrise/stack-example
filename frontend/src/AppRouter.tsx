import React from "react";
import { Route, Router, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import LoginPage from "./pages/login-page/LoginPage";
import HomePage from "./pages/HomePage";

export default function AppRouter() {
  return (
    <Router history={createBrowserHistory()}>
      <Switch>
        <Route exact path="/home" component={HomePage} />
        <Route component={LoginPage} />
      </Switch>
    </Router>
  );
}
