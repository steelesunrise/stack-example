import { createMuiTheme } from "@material-ui/core/styles";

// Material Theme
export const theme = createMuiTheme({
  palette: {
    common: {
      white: "#ffffff"
    },
    primary: {
      main: "#043c5f",
      dark: "#043553",
      light: "#083D55"
    },
    secondary: {
      main: "#266476",
      light: "#1897a0",
      dark: "#456f89"
    },
    error: {
      main: "#E6001F",
      light: "#ffcb3e"
    },
    text: {
      primary: "#707070",
      secondary: "#ffffff"
      // disabled: string;
      // hint: string;
    },
    background: {
      default: "#f8f8f8"
    }
  },
  typography: {
    fontFamily: "Lato, Helvetica, Arial, sans-serif",

    h1: {
      color: "#183041",
      fontSize: "1.625rem",
      fontWeight: 900
    },
    h2: {
      color: "#183041",
      fontSize: "1.125rem",
      textTransform: "uppercase",
      fontWeight: 900
    },
    h3: {
      color: "#183041",
      fontSize: "1rem",
      textTransform: "uppercase",
      fontWeight: 900
    },
    h4: {
      color: "#707070",
      fontSize: "0.875rem",
      fontWeight: 700
    },
    body1: {
      color: "#333333",
      fontSize: "0.875rem"
    },
    caption: {
      fontSize: "0.875rem",
      fontWeight: 300,
      fontStyle: "italic"
    }
  },
  overrides: {
    MuiTableSortLabel: {
      root: {
        "&:hover": {
          color: "inherit"
        },
        "&:focus": {
          color: "inherit"
        },
        "&$active": {
          color: "inherit",
          // && instead of & is a workaround for https://github.com/cssinjs/jss/issues/1045
          "&& $icon": {
            color: "inherit"
          }
        }
      }
    },
    MuiCheckbox: {
      root: {
        color: "inherit"
      }
    }
  }
});
