from marshmallow import Schema, ValidationError
from typing import Dict


class DomainSerializer:

    # Dumping (Objects to Dict or Json)

    @staticmethod
    def to_json(schema: Schema, model) -> str:
        try:
            json = schema.dumps(model)
        except ValidationError as err:
            print(err)
        return json

    @staticmethod
    def to_dict(schema: Schema, model) -> Dict:
        try:
            dic = schema.dump(model)
        except ValidationError as err:
            print(err)
        return dic

    # Loading (Creating particular objects)

    @staticmethod
    def json_to_obj(schema: Schema, data: str):
        return schema.loads(data)

    @staticmethod
    def dict_to_obj(schema: Schema, data: Dict):
        return schema.load(data)

    @staticmethod
    def obj_to_obj(schema_from: Schema, schema_to: Schema, model):
        temp_dict = DomainSerializer.to_dict(schema_from, model)
        model = DomainSerializer.dict_to_obj(schema_to, temp_dict)
        return model

    @staticmethod
    def camel_to_under(string: str) -> str:
        if string == "id":
            return "id_"
        if string == "type":
            return "type_"
        converted_string = ""
        for char in string:
            if char.isupper():
                converted_string = converted_string + "_" + char.lower()
            elif char.isdigit():
                converted_string = converted_string + "_" + char
            else:
                converted_string = converted_string + char
        return converted_string

    @staticmethod
    def under_to_camel(string: str) -> str:
        if string == "id_":
            return "id"
        if string == "type_":
            return "type"
        converted_string = ""
        capitalize_next = False
        for char in string:
            if char == "_":
                capitalize_next = True
            elif capitalize_next and not char.isdigit():
                converted_string = converted_string + char.upper()
                capitalize_next = False
            else:
                converted_string = converted_string + char
        return converted_string

    @staticmethod
    def keys_camel_to_under(data: Dict) -> Dict:
        new_data = {}
        for key in data:
            new_data[DomainSerializer.camel_to_under(key)] = data[key]
        return new_data

    @staticmethod
    def keys_under_to_camel(data: Dict) -> Dict:
        new_data = {}
        for key in data:
            new_data[DomainSerializer.under_to_camel(key)] = data[key]
        return new_data

