from flask import jsonify

from . import mock


@mock.route("/auth/hi", methods=["GET"])
def hi():
    res = {"message": "This is using the mock route"}

    return jsonify(res)
