from flask import current_app, jsonify

from example.external.pg import db_init

from . import default


@default.route("/config/initDb", methods=["GET"])
def init_db():
    print("Initializing the database.")
    db_init.init()
    res = {"success": 1, "message": "Database initialized"}
    return jsonify(res)
