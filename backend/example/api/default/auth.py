from flask import jsonify

from . import default


@default.route("/auth/hi", methods=["GET"])
def hi():
    res = {"message": "This is using the default route"}

    return jsonify(res)
