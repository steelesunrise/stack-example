from dataclasses import dataclass
from datetime import datetime


@dataclass
class AuthToken:
    user_id: int
    token: str
    issued_date: datetime
    revoked_date: datetime
    expiration_date: datetime


@dataclass
class Credentials:
    user_id: int
    username: str
    password: str
