from marshmallow import Schema, fields


class AuthTokenSchema(Schema):
    token = fields.Str()
    user_id = fields.Integer(data_key="userId")
    issued_date = fields.DateTime(data_key="issuedDate", allow_none=True)
    expiration_date = fields.DateTime(data_key="expirationDate", allow_none=True)
    revoked_date = fields.DateTime(data_key="revokedDate", allow_none=True)


# TODO VT: Add the other schemas like we do in WS
