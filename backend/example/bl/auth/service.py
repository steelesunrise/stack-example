import os
from datetime import datetime, timedelta
from hashlib import sha256

from werkzeug.security import check_password_hash, generate_password_hash

from .model import AuthToken, Credentials
from .repo import AuthRepo


class AuthService:
    def __init__(self):
        self.repo = AuthRepo()

    def new_token(self, user_id: int) -> AuthToken:
        token = self.repo.get_token_by_id(user_id)
        if token:
            self.revoke_token(token)

        token = self.generate_token(user_id)
        return self.repo.new_token(token)

    def get_token(self, token: str) -> AuthToken:
        return self.repo.get_token(token)

    def get_credentials(self, user_id: int = None, username: str = None) -> Credentials:
        creds = None
        if user_id:
            creds = self.repo.get_credentials_by_id(user_id)

        if username:
            creds = self.repo.get_credentials_by_username(username)

        return creds

    def revoke_token(self, auth_token: AuthToken) -> None:
        self.repo.revoke_token(auth_token)

    def check_credentials(self, credentials: Credentials, password) -> bool:
        validated = False
        if credentials:
            validated = self.check_password(credentials.password, password)
        return validated

    def check_password(self, user_password: str, provided_password: str) -> bool:
        return check_password_hash(user_password, provided_password)

    def hash_password(self, password: str) -> str:
        # In the future we could add a random hash we store in the database with the user as well
        return generate_password_hash(password)

    def generate_token(self, user_id: int) -> AuthToken:
        return AuthToken(
            token=sha256(os.urandom(60)).hexdigest(),
            user_id=user_id,
            issued_date=datetime.now(),
            expiration_date=datetime.now() + timedelta(hours=1),
            revoked_date=None,
        )
