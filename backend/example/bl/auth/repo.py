from typing import List, Dict
from datetime import datetime

from flask import current_app

from example.external.pg import example_session
from example.external.pg.schema import AuthToken as DbAuthToken
from example.external.pg.schema import Credentials as DbCredentials

from .model import AuthToken, Credentials


class AuthRepo:
    def __init__(self):
        pass

    def new_token(self, token: AuthToken) -> AuthToken:
        db_token = DbAuthToken(
            user_id=token.user_id,
            token=token.token,
            issued_date=token.issued_date,
            expiration_date=token.expiration_date,
            revoked_date=token.revoked_date,
        )
        example_session.add(db_token)
        example_session.commit()
        return self.get_token(token.token)

    def get_token(self, token: str) -> AuthToken:
        db_token = (
            example_session.query(DbAuthToken)
            .filter(DbAuthToken.token == token, DbAuthToken.revoked_date == None)
            .first()
        )
        return self.db_token_to_domain(db_token)

    def get_token_by_id(self, user_id: int) -> AuthToken:
        db_token = (
            example_session.query(DbAuthToken)
            .filter(DbAuthToken.user_id == user_id, DbAuthToken.revoked_date == None)
            .first()
        )
        current_app.logger.debug(db_token)
        return self.db_token_to_domain(db_token)

    def get_credentials_by_username(self, username: str) -> Credentials:
        db_credentials = (
            example_session.query(DbCredentials).filter(DbCredentials.username == username).first()
        )
        return self.db_credentials_to_domain(db_credentials)

    def get_credentials_by_id(self, user_id: int) -> Credentials:
        db_credentials = (
            example_session.query(DbCredentials).filter(DbCredentials.user_id == user_id).first()
        )
        return self.db_credentials_to_domain(db_credentials)

    def revoke_token(self, auth_token: AuthToken) -> None:
        if auth_token:
            token = DbAuthToken.query.filter(
                DbAuthToken.user_id == auth_token.user_id, DbAuthToken.token == auth_token.token
            ).first()
            token.revoked_date = datetime.now()
            example_session.commit()

    def db_token_to_domain(self, db_auth_token: DbAuthToken) -> AuthToken:
        if db_auth_token:
            return AuthToken(
                user_id=db_auth_token.user_id,
                token=db_auth_token.token,
                issued_date=db_auth_token.issued_date,
                revoked_date=db_auth_token.revoked_date,
                expiration_date=db_auth_token.expiration_date,
            )
        else:
            return None

    def db_credentials_to_domain(self, db_credentials: DbCredentials) -> Credentials:
        if db_credentials:
            return Credentials(
                user_id=db_credentials.user_id,
                username=db_credentials.username,
                password=db_credentials.password,
            )
        else:
            return None

