from dataclasses import dataclass

from example.domain.references.model import Lookup


@dataclass
class UserRole(Lookup):
    def __repr__(self):
        return f"UserRole(Code: {self.code}, Value: {self.value})"


@dataclass
class User:
    id_: int
    name: str
    email: str
    role: UserRole = None

    def __repr__(self):
        return f"User(Id: {self.id_}, Name: {self.name}, Role: {self.role})"
