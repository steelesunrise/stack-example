from marshmallow import Schema, fields, post_load, pre_load, post_dump

from .model import Lookup


class LookupSchema(Schema):
    code = fields.Integer()
    value = fields.String()

    @pre_load
    def pre_load(self, in_data, **kwargs):
        return in_data

    @post_load
    def post_load(self, data, **kwargs):
        return Lookup(**data)

    @post_dump
    def post_dump(self, data, **kwargs):
        return data
