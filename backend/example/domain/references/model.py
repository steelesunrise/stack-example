from dataclasses import dataclass


@dataclass
class Lookup:
    code: int
    value: str

    def __repr__(self):
        return f"Lookup(Code: {self.code}, Value: {self.value})"
