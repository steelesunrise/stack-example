import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

example_engine = create_engine(os.getenv("SQLALCHEMY_DB"))

ExampleBase = declarative_base()
ExampleBase.metadata.bind = example_engine

ExampleSession = scoped_session(sessionmaker(bind=example_engine))
example_session = ExampleSession()
