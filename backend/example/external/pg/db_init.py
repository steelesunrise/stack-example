import json

from example.bl.auth.service import AuthService

from example.constants import user_roles

from . import schema as dbs
from . import ExampleBase, example_session

auth_service: AuthService = AuthService()


def insert_credentials(name, creds):
    print(creds)
    user_id = example_session.query(dbs.User.id_).filter(dbs.User.name == name)
    db_creds = dbs.Credentials(
        user_id=user_id,
        username=creds["username"],
        password=auth_service.hash_password(creds["password"]),
    )
    example_session.add(db_creds)
    example_session.commit()


def insert_users(users):
    for u in users:
        print(u)
        db_user = dbs.User(name=u["name"], email=u["email"], role=u["role"])
        example_session.add(db_user)
        example_session.commit()
        insert_credentials(u["name"], u["credentials"])


def init():
    print(f"Dropping")
    ExampleBase.metadata.drop_all()
    print(f"Creating")
    ExampleBase.metadata.create_all()
    with open("db_init.json") as json_file:
        data = json.load(json_file)
        users = data["users"]
        insert_users(users)
