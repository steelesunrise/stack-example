from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean
from . import ExampleBase


class User(ExampleBase):
    __tablename__ = "user"

    id_ = Column(Integer, primary_key=True)
    name = Column(String(), nullable=False)
    email = Column(String(), nullable=False)
    role = Column(String(), nullable=False)


class AuthToken(ExampleBase):
    __tablename__ = "auth_token"

    user_id = Column(Integer, ForeignKey("user.id_"), primary_key=True)
    token = Column(String(), primary_key=True)
    issued_date = Column(DateTime(), nullable=False)
    expiration_date = Column(DateTime(), nullable=False)
    revoked_date = Column(DateTime(), nullable=True)


class Credentials(ExampleBase):
    __tablename__ = "credentials"

    user_id = Column(Integer, ForeignKey("user.id_"), primary_key=True)
    username = Column(String(), nullable=False)
    password = Column(String(), nullable=False)

