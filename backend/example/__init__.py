import os

from flask import Flask
from flask_cors import CORS

from example.config import config


def create_app(config_name):
    app = Flask(__name__)

    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    CORS(app, resources={r"*": {"origins": "*"}})

    from example.api.default import default as default_blueprint
    from example.api.mock import mock as mock_blueprint

    if config_name == "demo":
        app.register_blueprint(mock_blueprint)
    else:
        app.register_blueprint(default_blueprint)

    return app
