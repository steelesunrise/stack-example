import os
from dotenv import load_dotenv, find_dotenv

base_dir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(find_dotenv())


class Config:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BASE_URL = os.getenv("BASE_URL")

    @staticmethod
    def init_app(app):
        pass


class ProdConfig(Config):
    SQLALCHEMY_ECHO = False


class StagingConfig(Config):
    SQLALCHEMY_ECHO = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True


class DemoConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True


config = {
    "development": DevelopmentConfig,
    "staging": StagingConfig,
    "production": ProdConfig,
    "demo": DemoConfig,
    "default": DevelopmentConfig,
}
