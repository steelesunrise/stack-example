import os

from example import create_app

from example.external.pg import ExampleSession


app = create_app(os.getenv("FLASK_CONFIG") or "default")


@app.teardown_appcontext
def cleanup(resp_or_exc):
    ExampleSession.remove()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
